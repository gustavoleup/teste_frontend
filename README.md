# Consumindo API Youtube e Ticket Master

### Instalação

 * Execute `git clone https://gustavoleup@bitbucket.org/gustavoleup/teste_frontend.git `
 * Execute `cd laravel`
 * Execute `composer update`
 * Duplique o `.env.example` e renomeie para `.env`
 * Configure o seu `.env` com:

 * ```
 * YOUTUBE_API_KEY = SUA CHAVE DE ACESSO
 * TICKET_MASTER_API_KEY = SUA CHAVE DE ACESSO
 * ```
 * Voce pode conseguir as chaves de acesso em `https://developer-acct.ticketmaster.com/user/login?destination=user` e `https://console.developers.google.com/`

 * Execute `php artisan key:generate`

 * Inicie o servidor Laravel `php artisan serve`

 * Caso tenha problema com as API_KEYS, tente limpar o cache com o comando `php artisan config:cache` e depois `php artisan cache:clear`



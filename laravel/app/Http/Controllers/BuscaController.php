<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class BuscaController extends Controller
{
    /**
     * Mostra pagina inicial com a barra de busca.
     *
     * @param  /
     * @return /
     */

    public function index() {

        return view('search');
    }

    /**
     * Mostra mostra a pagina do resultado da busca.
     *
     * @param  @string
     * @return @json
     */

    public function search(Request $request) {

    $search = $request->input('parametrosBusca');

    $parms = str_replace(' ', '+', $request->input('parametrosBusca')); 
    // dd ($parms);
    $api_key_YouTube = config('api_keys.key_Y');
    $api_key_TicketMaster = config('api_keys.key_T');
    // dd ($api_key_YouTube);
    // dd ($api_key_TicketMaster);

    $urlApiYoutube = 'https://www.googleapis.com/youtube/v3/search?part=snippet&q=' . $parms . '&key=' . $api_key_YouTube;
    $urlApiTicketMaster = 'https://app.ticketmaster.com/discovery/v2/attractions.json?keyword=' . $parms .  '&apikey=' . $api_key_TicketMaster;
    
    $resposta = file_get_contents($urlApiYoutube);
    $resposta_tm = file_get_contents($urlApiTicketMaster);
    // dd ($resposta_tm);
    
    if ($resposta && $resposta_tm) {
        $api_data = json_decode($resposta);
        $api_data_T = json_decode($resposta_tm);
    } else {
        die('Falha na resposta');
    }
    // dd($api_data);
    // dd($api_data_T);

    // $por_page = $api_data->pageInfo->resultsPerPage = 1000;
    
    return view('results', compact ('api_data', 'api_data_T','search'));

    }

    /**
     * Mostra o video em pagina unica.
     *
     * @param  @string
     * @return @string
     */

    public function viewvideo($id)
    {
        // dd($id);

        return view ('viewVideo', compact('id'));
    }
}

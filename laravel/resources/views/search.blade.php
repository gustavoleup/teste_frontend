@extends('app')

@section('body')
<div class="container">
        <div class="row">
            <div class="col p-5">
            <h1 class="text-center">Procure por bandas e artistas.</h1>
            </div><!-- col -->
        </div><!-- row -->
        <div class="row">
            <div class="col p-5">
                <form action=" {{route('buscar')}} " method="post">
                    @csrf
                    <div class="input-group">
                       
                            <input type="text" name="parametrosBusca" class="form-control" placeholder="Pequisar" aria-label="Recipient's username" aria-describedby="button-addon2">
                            <button class="btn btn-outline-secondary" type="submit" id="button-addon2">Buscar</button>
                        
                    </div> <!-- input-group mb-3 -->
                </form>
            </div><!-- col -->
        </div><!-- row -->
        
    </div><!-- container -->
@endsection
@extends('app')
@section('body')

<style>
    .card.m-3 {
    display: inline-flex;
}
</style>

<div class="container">
    <div class="row">
            <div class="col p-5">
                <form action=" {{route('buscar')}} " method="post">
                    @csrf
                    <div class="input-group mb-3 form-check">
                       
                            <input type="text" name="parametrosBusca" class="form-control" placeholder="Pequisar" aria-label="Recipient's username" aria-describedby="button-addon2" autofocus>
                            <button class="btn btn-outline-secondary" type="submit" id="button-addon2">Buscar</button>
                        
                    </div> <!-- input-group mb-3 -->
                </form>
            </div><!-- col -->
        </div><!-- row -->
    <div class="row">
        <div class="col ms-10">
            <p class="text-center">
                Encontramos <strong>{{ $api_data->pageInfo->totalResults }}</strong> resultados para <strong><b>{{ $search }}</b></strong>.
            </p>
        </div><!-- col -->
    </div><!-- row -->
    <div class="row">
        <div class="col p-5">
        <ol class="list-group list-group-numbered">
            @foreach ($api_data_T->_embedded->attractions as $info)
            <li class="list-group-item d-flex justify-content-between align-items-start">
                <div class="ms-2 me-auto">
                <div class="fw-bold">{{ $info->name }}</div>
                @foreach ( $info->classifications as $genero)
                {{ $genero->genre->name }}
                @endforeach

                <p class="mb-1">Some placeholder content in a paragraph.</p>
                <small>And some small print.</small>
                </div>
                @foreach ( $info->classifications as $genero)
                <span class="badge bg-primary rounded-pill">{{ $genero->segment->name }}</span>
                @endforeach
            </li>
            @endforeach
        </ol>
        </div><!-- col -->
    </div><!-- row -->

    <div class="row">
        <div class="col p-5">
            @foreach ($api_data->items as $video)
                <div class="card m-3" style="width: 18rem;">
                    <img src="{{ $video->snippet->thumbnails->high->url }}" class="card-img-top" alt="{{ $video->snippet->title }}">
                    <!-- thumbnails -->
                    <div class="card-body">
                        <h5 class="card-title">{{ $video->snippet->title }}</h5>
                        <p class="card-text">{{ $video->snippet->description }}</p>
                        <a href="{{ route('viewVideo', $video->id->videoId )}}" class="btn btn-primary">Visualizar</a>
                    </div><!-- card-body -->
                </div><!-- card -->
            @endforeach 
            </div><!-- col -->
        </div><!-- row -->
</div><!-- container -->

          

@endsection
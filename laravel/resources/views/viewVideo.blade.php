@extends('app')

@section('body')
    <?php 
    // echo $idDoVideo 
    ?>
    
    <div class="container-fluid">
      <div class="row">
          <div class="col mt-5">
            <p class="lead">
              <a class="btn btn-primary" href="javascript:void(0)" onClick="history.go(-1); return false;" role="button">Voltar</a>
            </p>
          </div> <!-- col -->
      </div> <!-- row -->
      <div class="row">
        <div class="col">
          <div id="player" class="player-full">
              <!-- O <iframe> (e o player de vídeo) substituirá esta tag <div>. -->
          </div> <!-- id#player -->
        </div> <!-- col -->
      </div> <!-- row -->
    </div> <!-- container p-2 -->

    <script>
      // Este código carrega o código IFrame Player API de forma assíncrona
      var tag = document.createElement('script');

      tag.src = "https://www.youtube.com/iframe_api";
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

      // Esta função cria um <iframe> (e um player do YouTube) após o download do código API.
      var player;
      // var id = id do video do youtube.
      function onYouTubeIframeAPIReady() {
        player = new YT.Player('player', {
          height: 'auto',
          width: 'auto',
          videoId: '<?php echo $id ?>',
            // id dinamico do video.   
          events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
          }
        });
      }

      // A API chamará esta função quando o player de vídeo estiver pronto.
      function onPlayerReady(event) {
        event.target.playVideo();
      }

      // A API chama essa função quando o estado do player muda.
      // A função indica que ao reproduzir um vídeo (estado = 1),
      
      var done = false;
      function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.PLAYING && !done) {
          setTimeout(stopVideo, 6000);
          // O player deve tocar por seis segundos e depois parar.
          done = true;
        }
      }
      function stopVideo() {
        player.stopVideo();
      }
    </script>
@endsection
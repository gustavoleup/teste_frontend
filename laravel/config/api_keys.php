<?php

return [

    'key_Y' => env('YOUTUBE_API_KEY'), // Configuracao de API_KEY no .env

    'key_T' => env('TICKET_MASTER_API_KEY'), // Configuracao de API_KEY no .env
];